package com.globant.training.jenkinsmavengit.services;

import org.springframework.stereotype.Service;

@Service
public class GreetingService implements IGreetingsService {

	@Override
	public String greet(String name) {
		String greet = "Hello";
		
		Operacion operacion = (s1, s2) ->  s1.trim() + " " + s2.trim() + "!";
		
		System.out.println("paso interface");
		
		return operacion.concatenar(greet, name);
	}	
	

}
