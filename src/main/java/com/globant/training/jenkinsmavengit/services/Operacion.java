package com.globant.training.jenkinsmavengit.services;

@FunctionalInterface
public interface Operacion {
	
	String concatenar(String s1, String s2);

}
