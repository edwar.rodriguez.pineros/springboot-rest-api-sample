package com.globant.training.jenkinsmavengit.services;

public interface IGreetingsService {

	String greet(String name);
	
}
