package com.globant.training.jenkinsmavengitdto;

public class Persona {

	private String tipoId;
	private String numeroId;
	
	private String nombre;
	private String tel;
	
	
	
	public Persona() {
		super();
	}
	
	public Persona(String tipoId, String numeroId, String nombre, String tel) {
		super();
		this.tipoId = tipoId;
		this.numeroId = numeroId;
		this.nombre = nombre;
		this.tel = tel;
	}
	public String getTipoId() {
		return tipoId;
	}
	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}
	public String getNumeroId() {
		return numeroId;
	}
	public void setNumeroId(String numeroId) {
		this.numeroId = numeroId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroId == null) ? 0 : numeroId.hashCode());
		result = prime * result + ((tipoId == null) ? 0 : tipoId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (numeroId == null) {
			if (other.numeroId != null)
				return false;
		} else if (!numeroId.equals(other.numeroId))
			return false;
		if (tipoId == null) {
			if (other.tipoId != null)
				return false;
		} else if (!tipoId.equals(other.tipoId))
			return false;
		return true;
	}
	
	
	
}
